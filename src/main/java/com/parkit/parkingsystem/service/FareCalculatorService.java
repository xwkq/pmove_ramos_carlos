package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.model.Ticket;


public class FareCalculatorService {



    public double calculateFareCareFreeHalfHour(double inTime, double outTime){
        double duration = (outTime - inTime) / 3600;
        duration -= 0.30 * 0.60;
        duration = duration * 0.001;
        duration = Math.max(0, duration);
        return duration;
    }

    public void calculateFareCareLess5Percent(double price, Ticket ticket, int regNumber){
        if(regNumber >= 2){
            price -= 5 * price / 100;
            ticket.setPrice(price);
        }
    }

    public void calculateFare(Ticket ticket, int regNumber){
        if( (ticket.getOutTime() == null) || (ticket.getOutTime().before(ticket.getInTime())) ){
            throw new IllegalArgumentException("Out time provided is incorrect:"+ticket.getOutTime().toString());
        }

        double inTime = ticket.getInTime().getTime();
        double outTime = ticket.getOutTime().getTime();

        double duration;
        duration = calculateFareCareFreeHalfHour(inTime, outTime);

        switch (ticket.getParkingSpot().getParkingType()){
            case CAR: {
                ticket.setPrice(duration * Fare.CAR_RATE_PER_HOUR);
                break;
            }
            case BIKE: {
                ticket.setPrice(duration * Fare.BIKE_RATE_PER_HOUR);
                break;
            }
            default: throw new IllegalArgumentException("Unkown Parking Type");
        }

        calculateFareCareLess5Percent(ticket.getPrice(), ticket, regNumber);
    }
}