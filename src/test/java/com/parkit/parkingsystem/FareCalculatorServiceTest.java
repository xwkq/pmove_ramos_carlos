package com.parkit.parkingsystem;

import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.FareCalculatorService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;
import java.util.Date;

public class FareCalculatorServiceTest {

    private static FareCalculatorService fareCalculatorService;
    private Ticket ticket;

    @BeforeAll
    public static void setUp() {
        fareCalculatorService = new FareCalculatorService();
    }

    @BeforeEach
    public void setUpPerTest() {
        ticket = new Ticket();
    }

    @Test
    public void calculateFareCar(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  60 * 60 * 1000) );
        Date outTime = new Date();
        Timestamp tsInTime = new Timestamp(inTime.getTime());
        Timestamp tsOuTime = new Timestamp(outTime.getTime());
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR,false);

        ticket.setInTime(tsInTime);
        ticket.setOutTime(tsOuTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        assertEquals(Math.round(ticket.getPrice() * 100.0) / 100.0, Fare.CAR_RATE_PER_HOUR);
    }

    @Test
    public void calculateFareBike(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  60 * 60 * 1000) );
        Date outTime = new Date();
        Timestamp tsInTime = new Timestamp(inTime.getTime());
        Timestamp tsOuTime = new Timestamp(outTime.getTime());
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.BIKE,false);

        ticket.setInTime(tsInTime);
        ticket.setOutTime(tsOuTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        assertEquals(Math.round(ticket.getPrice() * 100.0) / 100.0, Fare.BIKE_RATE_PER_HOUR);
    }

    @Test
    public void calculateFareUnknownType(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  60 * 60 * 1000) );
        Date outTime = new Date();
        ParkingSpot parkingSpot = new ParkingSpot(1, null,false);

        ticket.setInTime(inTime);
        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        assertNull(parkingSpot.getParkingType());
        assertThrows(NullPointerException.class, () -> fareCalculatorService.calculateFare(ticket, 1));
    }

    @Test
    public void calculateFareBikeWithFutureInTime(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() + (  60 * 60 * 1000) );
        Date outTime = new Date();
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.BIKE,false);

        ticket.setInTime(inTime);
        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        assertThrows(IllegalArgumentException.class, () -> fareCalculatorService.calculateFare(ticket, 1));
    }

    @Test
    public void calculateFareBikeWithLessThanOneHourParkingTime(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  45 * 60 * 1000) );//45 minutes parking time should give 3/4th parking fare
        Date outTime = new Date();
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.BIKE,false);

        ticket.setInTime(inTime);
        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        assertEquals((0.75 * Fare.BIKE_RATE_PER_HOUR), Math.round(ticket.getPrice() * 100.0) / 100.0 );
    }

    @Test
    public void calculateFareCarWithLessThanOneHourParkingTime(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  45 * 60 * 1000) );//45 minutes parking time should give 3/4th parking fare
        Date outTime = new Date();
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR,false);

        ticket.setInTime(inTime);
        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        assertEquals( (0.75 * Fare.CAR_RATE_PER_HOUR) , Math.round(ticket.getPrice() * 1000.0) / 1000.0);
    }

    @Test
    public void calculateFareCarWithMoreThanADayParkingTime(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  24 * 60 * 60 * 1000) );//24 hours parking time should give 24 * parking fare per hour
        Date outTime = new Date();
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR,false);

        ticket.setInTime(inTime);
        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        assertEquals( (24 * Fare.CAR_RATE_PER_HOUR) , Math.round(ticket.getPrice() * 100.0) / 100.0);
    }

    @Test
    public void calculateFareCareLess5PercentTest(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  60 * 60 * 1000) );
        Date outTime = new Date();
        Timestamp tsInTime = new Timestamp(inTime.getTime());
        Timestamp tsOuTime = new Timestamp(outTime.getTime());
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR,false);
        ticket.setInTime(tsInTime);
        ticket.setOutTime(tsOuTime);
        ticket.setVehicleRegNumber("12345");
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        double oldPrice = ticket.getPrice();
        fareCalculatorService.calculateFare(ticket, 3);
        assertNotEquals(oldPrice, ticket.getPrice());
        oldPrice -= 5 * oldPrice / 100;
        assertEquals(oldPrice, ticket.getPrice());
    }

    @Test
    public void calculateFareCareFreeHalfHourTest(){
        Date inTime = new Date();
        inTime.setTime( System.currentTimeMillis() - (  30 * 60 * 1000) );
        Date outTime = new Date();
        Timestamp tsInTime = new Timestamp(inTime.getTime());
        Timestamp tsOuTime = new Timestamp(outTime.getTime());
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR,false);
        ticket.setInTime(tsInTime);
        ticket.setOutTime(tsOuTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket, 1);
        assertEquals(Fare.CAR_RATE_PER_HALF_HOUR, Math.round(ticket.getPrice() * 100.0) / 100.0);
    }

}
