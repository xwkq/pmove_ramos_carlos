package com.parkit.parkingsystem.dao;

import com.parkit.parkingsystem.integration.ParkingDataBaseIT;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TicketDAOTest {

    private static final DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private static ParkingSpotDAO parkingSpotDAO;
    private static TicketDAO ticketDAO;
    private static DataBasePrepareService dataBasePrepareService;

    @Mock
    public static InputReaderUtil inputReaderUtil;

    @BeforeAll
    public static void setUp(){
        parkingSpotDAO = new ParkingSpotDAO();
        parkingSpotDAO.dataBaseConfig = dataBaseTestConfig;
        ticketDAO = new TicketDAO();
        ticketDAO.dataBaseConfig = dataBaseTestConfig;
        dataBasePrepareService = new DataBasePrepareService();
    }

    @BeforeEach
    public void setUpPerTest() throws Exception {
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        dataBasePrepareService.clearDataBaseEntries();
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);
        parkingService.processIncomingVehicle();
    }

    @Test
    void saveTicket() {
        Ticket newTicket = ticketDAO.getTicket("ABCDEF");
        newTicket.setVehicleRegNumber("TEST");
        int idNewTicket = newTicket.getId();
        ticketDAO.saveTicket(newTicket);
        Ticket finalTicket = ticketDAO.getTicket("TEST");
        int idFinalTicket = finalTicket.getId();
        assertNotNull(finalTicket);
        assertTrue(idNewTicket != idFinalTicket);
    }


    @Test
    void updateTicket() {
        Ticket newTicket = ticketDAO.getTicket("ABCDEF");
        String oldReg = newTicket.getVehicleRegNumber();
        newTicket.setVehicleRegNumber("UpdateREG");
        ticketDAO.updateTicket(newTicket);
        String newReg = newTicket.getVehicleRegNumber();
        assertNotEquals(newReg, oldReg);
    }

}